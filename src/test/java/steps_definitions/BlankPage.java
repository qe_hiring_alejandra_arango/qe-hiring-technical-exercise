package steps_definitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data_pool.Data;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.*;
import support.automationWebDriver;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;


public final class BlankPage extends automationWebDriver {

    public WebDriver driver;

    int numberOfCreatedPages;
    int numberOfDraftPages;
    String draftTitle;

    @Before("@blankPage")
    public void setUp() {
        driver = open_url(Data.url);
    }

    @After("@blankPage")
    public void tearDown() {
        clear_status();
    }

    @Given("^An user logged into Confluence space$")
    public void an_user_logged_into_confluence_space() {
        SignIn signIn = new SignIn(driver);
        signIn.clickOnGoogleSignInButton();
        signIn.setGoogleUserName(Data.userName);
        signIn.clickOnIdContinueButton();
        signIn.setGooglePassword(Data.password);
        signIn.clickOnGooglePasswordContinueButton();
    }


    @When("^The user creates and publishes a new blank page$")
    public void theUserCratesANewBlankPage() {
        GlobalConfluenceMenu GlobalConfluenceMenu = new GlobalConfluenceMenu(driver);
        GlobalConfluenceMenu.clickOnSpacesButton();
        ConfluenceSpaceDirectory confluenceSpaceDirectory = new ConfluenceSpaceDirectory(driver);
        confluenceSpaceDirectory.clickOnDefaultSpace();
        ConfluenceSpaceMenu confluenceSpaceMenu = new ConfluenceSpaceMenu(driver);
        numberOfCreatedPages = confluenceSpaceMenu.createdPagesQuantity();
        GlobalConfluenceMenu.clickOnCreateButton();
        CreateModal createModal = new CreateModal(driver);
        createModal.clickOnBlankPageButton();
        createModal.clickOnCreateModalButton();
        ConfluencePage confluencePage = new ConfluencePage(driver);
        confluencePage.setConfluencePageTitle(Data.blankPageTitle + numberOfCreatedPages);
        confluencePage.clickOnPublishButton();
    }

    @Then("^The page should appear on the space pages section$")
    public void thePageShouldAppearOnTheSpacePagesSection() {
        boolean isPageCreated = false;
        ConfluenceSpaceMenu confluenceSpaceMenu = new ConfluenceSpaceMenu(driver);
        assertEquals("The page was not created", numberOfCreatedPages + 1, confluenceSpaceMenu.createdPagesQuantity());
        for (WebElement pages : confluenceSpaceMenu.createdPagesList()) {
            if (pages.getText().contains(String.valueOf(numberOfCreatedPages))) {
                isPageCreated = true;
                break;
            }
        }
        assertTrue(isPageCreated);
    }

    @When("^The user creates and closes a new blank page$")
    public void theUserCreatesAndClosesANewBlankPage() {
        GlobalConfluenceMenu globalConfluenceMenu = new GlobalConfluenceMenu(driver);
        globalConfluenceMenu.clickYourWorkButton();
        YourWork yourWork = new YourWork(driver);
        yourWork.clickOnDraftLink();
        numberOfDraftPages = yourWork.createdDraftsQuantity();
        globalConfluenceMenu.clickOnCreateButton();
        CreateModal createModal = new CreateModal(driver);
        createModal.clickOnBlankPageButton();
        createModal.clickOnCreateModalButton();
        ConfluencePage confluencePage = new ConfluencePage(driver);
        draftTitle = Data.blankPageTitle + numberOfDraftPages;
        confluencePage.setConfluencePageTitle(draftTitle);
        confluencePage.clickOnCloseButton();
    }


    @Then("^The draft should appear on 'My Work' section$")
    public void theDraftShouldAppearOnMyWorkSection() {
        YourWork yourWork = new YourWork(driver);
        yourWork.clickOnDraftLink();
        assertTrue(yourWork.isDraftPageVisible(draftTitle));
    }

    @When("^The user creates and publishes a new blank page without a title$")
    public void theUserCreatesAndPublishesANewBlankPageWithoutATitle() {
        GlobalConfluenceMenu globalConfluenceMenu = new GlobalConfluenceMenu(driver);
        globalConfluenceMenu.clickOnCreateButton();
        CreateModal createModal = new CreateModal(driver);
        createModal.clickOnBlankPageButton();
        createModal.clickOnCreateModalButton();
        ConfluencePage createPage = new ConfluencePage(driver);
        createPage.clickOnPublishButton();
    }

    @Then("^Confluence should show an error message$")
    public void confluenceShouldShowAnErrorMessage() {
        ConfluencePage confluencePage = new ConfluencePage(driver);
        assertEquals("This page needs a name Add a page title before hitting publish.", confluencePage.getErrorAlertMessage().replace("\n", " "));
    }

    @Given("^Exist a publish page on Confluence Space$")
    public void existsAPreviousCreatedPublishPage() {
        SignIn signIn = new SignIn(driver);
        signIn.clickOnGoogleSignInButton();
        signIn.setGoogleUserName(Data.userName);
        signIn.clickOnIdContinueButton();
        signIn.setGooglePassword(Data.password);
        signIn.clickOnGooglePasswordContinueButton();
        GlobalConfluenceMenu globalConfluenceMenu = new GlobalConfluenceMenu(driver);
        globalConfluenceMenu.clickOnSpacesButton();
        ConfluenceSpaceDirectory confluenceSpaceDirectory = new ConfluenceSpaceDirectory(driver);
        confluenceSpaceDirectory.clickOnDefaultSpace();
        ConfluenceSpaceMenu confluenceSpaceMenu = new ConfluenceSpaceMenu(driver);
        if (!globalConfluenceMenu.isAGivenPageDisplayed(Data.createdPageForRestrictionsTest)) {
            globalConfluenceMenu.clickOnCreateButton();
            CreateModal createModal = new CreateModal(driver);
            createModal.clickOnBlankPageButton();
            createModal.clickOnCreateModalButton();
            ConfluencePage createPage = new ConfluencePage(driver);
            createPage.setConfluencePageTitle(Data.createdPageForRestrictionsTest);
            createPage.clickOnPublishButton();
        } else {
            confluenceSpaceMenu.clickOnPageGivenTitle(Data.createdPageForRestrictionsTest);
        }
    }

    @When("^The user sets 'No restrictions' for the page$")
    public void theUserSetsNoRestrictionsForThePage() {

    }

    @When("^The user sets \"([^\"]*)\" for the page$")
    public void theUserSetsForThePage(String restrictionType) {
        ConfluencePage confluencePage = new ConfluencePage(driver);
        confluencePage.clickOnRestrictionsButton();
        PageRestrictionsModal pageRestrictionsModal = new PageRestrictionsModal(driver);
        if (pageRestrictionsModal.getRestrictionSelectedOption().equals(restrictionType)) {
            pageRestrictionsModal.clickOnCancelRestrictionButton();
        } else {
            pageRestrictionsModal.selectRestrictionOption(restrictionType);
            pageRestrictionsModal.clickOnApplyRestrictionButton();
        }
    }

    @Then("^Everyone can view and edit this page$")
    public void everyoneCanViewAndEditThisPage() {
        navigationToPageWithDifferentUser();
        GlobalConfluenceMenu GlobalConfluenceMenu = new GlobalConfluenceMenu(driver);
        ConfluenceSpaceMenu confluenceSpaceMenu = new ConfluenceSpaceMenu(driver);
        assertTrue(GlobalConfluenceMenu.isAGivenPageDisplayed(Data.createdPageForRestrictionsTest));
        confluenceSpaceMenu.clickOnPageGivenTitle(Data.createdPageForRestrictionsTest);
        ConfluencePage confluencePage = new ConfluencePage(driver);
        assertTrue(confluencePage.isEditIconPresent());
    }

    @Then("^Everyone can view only some can edit$")
    public void everyoneCanViewOnlySomeCanEdit() {
        navigationToPageWithDifferentUser();
        GlobalConfluenceMenu globalConfluenceMenu = new GlobalConfluenceMenu(driver);
        ConfluenceSpaceMenu ConfluenceSpaceMenu = new ConfluenceSpaceMenu(driver);
        assertTrue(globalConfluenceMenu.isAGivenPageDisplayed(Data.createdPageForRestrictionsTest));
        ConfluenceSpaceMenu.clickOnPageGivenTitle(Data.createdPageForRestrictionsTest);
        ConfluencePage confluencePage = new ConfluencePage(driver);
        assertFalse(confluencePage.isEditIconPresent());

    }

    @Then("^Only some people can view or edit$")
    public void onlySomePeopleCanViewOrEdit() {
        navigationToPageWithDifferentUser();
        GlobalConfluenceMenu GlobalConfluenceMenu = new GlobalConfluenceMenu(driver);
        assertFalse(GlobalConfluenceMenu.isAGivenPageDisplayed(Data.createdPageForRestrictionsTest));
    }

    /**
     * Navigation to pages with a different user
     */
    private void navigationToPageWithDifferentUser() {
        GlobalConfluenceMenu globalConfluenceMenu = new GlobalConfluenceMenu(driver);
        driver.navigate().refresh();
        globalConfluenceMenu.clickSystemUserIcon();
        globalConfluenceMenu.clickOnLogOutButton();
        SignIn signIn = new SignIn(driver);
        signIn.clickOnLogoutConfirmationButton();
        driver.navigate().to(Data.url);
        signIn.setConfluenceUserName(Data.testUserName);
        signIn.clickOnContinueButton();
        signIn.setConfluencePassword(Data.password);
        signIn.clickOnContinueButton();
        GlobalConfluenceMenu GlobalConfluenceMenu = new GlobalConfluenceMenu(driver);
        GlobalConfluenceMenu.clickOnSpacesButton();
        ConfluenceSpaceDirectory spaceDirectory = new ConfluenceSpaceDirectory(driver);
        spaceDirectory.clickOnDefaultSpace();
    }

}
