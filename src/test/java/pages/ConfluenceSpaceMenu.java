package pages;

import locators.Locators;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class ConfluenceSpaceMenu extends QEHiringPage {

    /**
     * Constructor
     * @param driver Web Driver
     */
    public ConfluenceSpaceMenu(WebDriver driver) {
        super(driver);
    }

    /**
     * Dynamic locator for a specific page based on a given title
     */
    private static final String givenPageTitle = "//div[contains(text(),'%s')]";

    @FindBy(xpath = Locators.firstPage)
    private WebElement firstSpacePage;

    @FindBy(xpath = Locators.createdPagesList)
    private List<WebElement> createdPagesList;

    @FindBy(xpath = Locators.firstCreatedPage)
    private WebElement firstCreatedPage;

    /**
     * Returns the quantity of created pages
     * @return Quantity of created pages
     */
    public int createdPagesQuantity(){
        return createdPagesList.size();
    }

    /**
     * Clicks on firts page
     */
    public void clickOnFirstSpacePage(){
        clickOnElement(firstCreatedPage);
    }
    /**
     * Returns created pages list
     * @return Created pages list
     */
    public List<WebElement> createdPagesList(){
        return createdPagesList;
    }

    /**
     * Click on page given a title
     * @param titlePage page title to click
     */
    public void clickOnPageGivenTitle (String titlePage){
        WebElement confluencePage = driver.findElement(By.xpath(String.format(givenPageTitle, titlePage)));
        clickOnElement(confluencePage);
    }
}
