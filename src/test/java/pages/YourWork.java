package pages;

import locators.Locators;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class YourWork extends QEHiringPage {

    //Constructor
    public YourWork(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = Locators.draftsLink)
    private WebElement draftsLink;

    @FindBy(xpath = Locators.createdDrafts)
    private List<WebElement> createdDrafts;
    /**
     * Draft Page Link given title draft page
     */
    private static final String draftPageLink = "//p[contains(text(),'%s')]";

    /**
     * Clicks on "Drafts" Link
     */
    public void clickOnDraftLink() {
        clickOnElement(draftsLink);
    }

    /**
     * Returns the quantity of created draft pages
     *
     * @return Quantity of created draft pages
     */
    public int createdDraftsQuantity() {
        // wait.until(ExpectedConditions.visibilityOfAllElements(createdDrafts));
        return createdDrafts.size();
    }

    /**
     * Return if a given draft page is visible
     *
     * @param draftTitle Draft Title
     * @return True if is visible, false otherwise
     */
    public boolean isDraftPageVisible(String draftTitle) {
        return isWebElementPresent(By.xpath(String.format(draftPageLink, draftTitle)));
    }
}





