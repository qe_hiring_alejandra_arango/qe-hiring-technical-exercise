package pages;

import locators.Locators;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class GlobalConfluenceMenu extends QEHiringPage {
    /**
     * Confluence Page on Confluence Menu
     */
    private static final String confluencePage = "//div[contains(text(), '%s')]";


    @FindBy(css = Locators.createPageButton)
    private WebElement createButton;

    @FindBy(xpath = Locators.spacesButton)
    private WebElement spacesButton;

    @FindBy(xpath = Locators.YourWorkButton)
    private WebElement yourWorkLink;

    @FindBy(css = Locators.systemUserButton)
    private WebElement systemUSerButton;

    @FindBy(css = Locators.logOutButton)
    private WebElement logOutButton;


    /**
     * Constructor
     *
     * @param driver Web Driver
     */
    public GlobalConfluenceMenu(WebDriver driver) {
        super(driver);
    }

    /**
     * Clicks on "Create" button
     */
    public void clickOnCreateButton() {
        clickOnElement(createButton);
    }

    /**
     * Clicks On Spaces Button
     */
    public void clickOnSpacesButton() {
        clickOnElement(spacesButton);
    }

    /**
     * Clicks on "Your Work" button
     */
    public void clickYourWorkButton() {
        clickOnElement(yourWorkLink);
    }

    /**
     * Clicks on "System User" button
     */
    public void clickSystemUserIcon() {
        clickOnElement(systemUSerButton);
    }

    /**
     * Clicks on "Log Out" button
     */
    public void clickOnLogOutButton() {
        clickOnElement(logOutButton);
    }

    /**
     * Evaluates if a specific "Page" is shown given a title Page
     *
     * @param pageTitle Oage Title of page to be evaluated
     * @return True if the page is shown, False otherwise
     */
    public boolean isAGivenPageDisplayed(String pageTitle) {
        return isWebElementPresent(By.xpath(String.format(confluencePage, pageTitle)));
    }
}
