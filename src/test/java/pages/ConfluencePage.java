package pages;

import locators.Locators;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class ConfluencePage extends QEHiringPage {

    /**
     * Constructor
     *
     * @param driver Web Driver
     */
    public ConfluencePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = Locators.confluencePageTitleInput)
    private WebElement confluencePageTitleInput;

    @FindBy(css = Locators.publishButton)
    private WebElement publishButton;

    @FindBy(css = Locators.closeButton)
    private WebElement closeButton;

    @FindBy(css = Locators.saveChangesIndicator)
    private WebElement saveChangesIndicator;

    @FindBy(css = Locators.errorAlertMessage)
    private WebElement errorAlertMessage;

    @FindBy(css = Locators.restrictionsIcon)
    private WebElement restrictionsIcon;

    @FindBy(css = Locators.editIcon)
    private WebElement editIcon;

    @FindBy(css = Locators.deleteConfirmationButton)
    private WebElement deleteConfirmationButton;


    /**
     * Sets the title for a Confluence page
     *
     * @param title Title to be set
     */
    public void setConfluencePageTitle(String title) {
        typesIntoElement(confluencePageTitleInput, title);
    }

    /**
     * Gets the error alert message
     *
     * @return The error alert message
     */
    public String getErrorAlertMessage() {
        return getElementText(errorAlertMessage);
    }

    /**
     * Clicks on "Publish" button
     */
    public void clickOnPublishButton() {
        wait.until(ExpectedConditions.textToBePresentInElement(saveChangesIndicator, "Ready to go"));
        clickOnElement(publishButton);
    }

    /**
     * Click on Close button
     */
    public void clickOnCloseButton() {
        wait.until(ExpectedConditions.textToBePresentInElement(saveChangesIndicator, "Ready to go"));
        clickOnElement(closeButton);
    }

    /**
     * Clicks on "Restriction" icon
     */
    public void clickOnRestrictionsButton() {
        clickOnElement(restrictionsIcon);
    }

    /**
     * Clicks on "Confirmation" delete button
     */
    public void clickOnDeleteConfirmationButton() {
        clickOnElement(deleteConfirmationButton);
    }

    /**
     * Evaluates if Edit Icon is shown on the page
     *
     * @return True if is shown, False otherwise
     */
    public boolean isEditIconPresent() {
        return isWebElementPresent(By.cssSelector(Locators.editIcon));
    }

}
