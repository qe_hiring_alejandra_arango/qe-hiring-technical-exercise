package pages;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageEllipsisMenu extends QEHiringPage{
    /**
     * Constructor
     *
     * @param driver Web driver
     */
    public PageEllipsisMenu(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = Locators.pageEllipsisMenuButton)
    private WebElement pageEllipsisMenuButton;

    @FindBy(css = Locators.deletePageButton)
    private WebElement deletePageButton;

    /**
     * Clicks on Page Ellipsis button
     */
    public void clickOnEllipsisButton(){
        clickOnElement(pageEllipsisMenuButton);
    }

    /**
     * Clicks on Delete button
     */
    public void clickOnDeleteButton(){
        clickOnElement(deletePageButton);
    }

}
