package pages;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignIn extends QEHiringPage {


    @FindBy(css = Locators.googleUserNameTextbox)
    private WebElement googleUserNameTextbox;

    @FindBy(css = Locators.userNameConfluenceTextBox)
    private WebElement userNameConfluenceTextBox;

    @FindBy(css = Locators.googleIdContinueButton)
    private WebElement googleIdContinueButton;

    @FindBy(css = Locators.googleSignInButton)
    private WebElement googleSignInButton;

    @FindBy(css = Locators.googlePasswordTextbox)
    private WebElement googlePasswordTextbox;

    @FindBy(css = Locators.confluenceUserPasswordLogin)
    private WebElement confluenceUserPasswordLogin;

    @FindBy(css = Locators.googlePasswordContinueButton)
    private WebElement googlePasswordContinueButton;

    @FindBy(css = Locators.logOutConfirmationButton)
    private WebElement logOutConfirmationButton;

    @FindBy(css = Locators.continueButton)
    private WebElement continueButton;

    /**
     * Constructor
     *
     * @param driver
     */
    public SignIn(WebDriver driver) {
        super(driver);
    }

    /**
     * Sets google username
     *
     * @param userName Google User Name
     */
    public void setGoogleUserName(String userName) {
        typesIntoElement(googleUserNameTextbox, userName);
    }

    /**
     * Sets Confleucne user Name
     *
     * @param userName Confluence User Name
     */
    public void setConfluenceUserName(String userName) {
        typesIntoElement(userNameConfluenceTextBox, userName);
    }

    /**
     * Clicks on Google ID Continue button
     */
    public void clickOnIdContinueButton() {
        clickOnElement(googleIdContinueButton);
    }

    /**
     * Clicks on Google Sign ID Button
     */
    public void clickOnGoogleSignInButton() {
        clickOnElement(googleSignInButton);
    }

    /**
     * Clicks on Continue Button
     */
    public void clickOnContinueButton() {
        clickOnElement(continueButton);
    }

    /**
     * Sets Google password
     *
     * @param password Google Password
     */
    public void setGooglePassword(String password) {
        typesIntoElement(googlePasswordTextbox, password);
    }

    /**
     * Sets Confluence Password
     *
     * @param password Confluence Password
     */
    public void setConfluencePassword(String password) {
        typesIntoElement(confluenceUserPasswordLogin, password);
    }

    /**
     * Clicks on google password continue button
     */
    public void clickOnGooglePasswordContinueButton() {
        clickOnElement(googlePasswordContinueButton);
    }

    /**
     * Clicks on Log out confirmation button
     */
    public void clickOnLogoutConfirmationButton() {
        clickOnElement(logOutConfirmationButton);
    }


}
