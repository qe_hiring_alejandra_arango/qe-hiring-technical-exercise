package pages;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConfluenceSpaceDirectory extends QEHiringPage{
    /**
     * Constructor
     * @param driver Web Driver
     */
    public ConfluenceSpaceDirectory(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = Locators.QEHiringSpace)
    private WebElement QEHiringSpace;

    /**
     * Click on "WE Hiring" Space
     */
    public void clickOnDefaultSpace(){
        clickOnElement(QEHiringSpace);
    }
}
