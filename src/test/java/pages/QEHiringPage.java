package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class QEHiringPage {

    WebDriver driver;
    WebDriverWait wait;

    /**
     * Constructor
     *
     * @param driver Web driver
     */
    public QEHiringPage(WebDriver driver) {
        this.driver = driver;
        //Page factory
        PageFactory.initElements(this.driver, this);
        //Web Driver Wait
        wait = new WebDriverWait(this.driver, 30);

    }

    /**
     * Clicks on a given web element
     *
     * @param element Web Element to be clackable
     */
    public void clickOnElement(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    /**
     * Types a given text on a Web Element
     *
     * @param element Element to be typed
     * @param text    Text to be set
     */
    public void typesIntoElement(WebElement element, String text) {
        wait.until(ExpectedConditions.visibilityOf(element));
        element.sendKeys(text);
    }

    /**
     * Gets element text
     *
     * @param element Element to be evaluated
     * @return Element Text
     */
    public String getElementText(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.getText();
    }

    /**
     * Returns If a given element is visible
     *
     * @param by Selector element
     * @return True if the element is visible, False otherwise
     */
    public boolean isWebElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }


}
