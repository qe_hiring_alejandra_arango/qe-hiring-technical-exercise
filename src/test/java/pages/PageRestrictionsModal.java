package pages;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class PageRestrictionsModal extends QEHiringPage {
    /**
     * Constructor
     *
     * @param driver Web Driver
     */
    public PageRestrictionsModal(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = Locators.restrictionsTypeDropdown)
    private WebElement restrictionsTypeDropdown;

    @FindBy(css = Locators.applyRestrictionButton)
    private WebElement applyRestrictionsButton;

    @FindBy(css = Locators.cancelRestrictionButton)
    private WebElement cancelRestrictionButton;

    /**
     * Selects a restriction given a restriction type
     *
     * @param restrictionType Restriction Type to be selected
     */
    public void selectRestrictionOption(String restrictionType) {
        Select restrictionsDropdown = new Select(restrictionsTypeDropdown);
        restrictionsDropdown.selectByVisibleText(restrictionType);
    }

    /**
     * Gets "Restriction" selected option
     *
     * @return Restriction selected option
     */
    public String getRestrictionSelectedOption() {
        Select restrictionsDropdown = new Select(restrictionsTypeDropdown);
        return restrictionsDropdown.getFirstSelectedOption().getText();
    }

    /**
     * Clicks on "Apply" restriction button
     */
    public void clickOnApplyRestrictionButton() {
        clickOnElement(applyRestrictionsButton);
    }

    /**
     * Clicks on "Cancel" registration button
     */
    public void clickOnCancelRestrictionButton() {
        clickOnElement(cancelRestrictionButton);
    }
}
