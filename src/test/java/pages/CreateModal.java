package pages;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CreateModal extends QEHiringPage{
    /**
     * Constructor
     * @param driver Web driver
     */
    public CreateModal(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = Locators.newBlankPageButton)
    private WebElement blankPageButton;

    @FindBy(css = Locators.createModalButton)
    private WebElement createModalButton;

    /**
     * Clicks on Blank Page button
     */
    public void clickOnBlankPageButton (){
        clickOnElement(blankPageButton);
    }

    /**
     * Clicks on "Create" modal button
     */
    public void clickOnCreateModalButton (){
        clickOnElement(createModalButton);
    }
}
