package locators;

public class Locators {
    //Sign in locators
    public final static String googleSignInButton = "button[id='google-signin-button']";
    public final static String googleUserNameTextbox = "input[id='identifierId']";
    public final static String userNameConfluenceTextBox = "input[id='username']";
    public final static String confluenceUserPasswordLogin = "input[id='password']";
    public final static String continueButton = "button[id='login-submit']";
    public final static String googlePasswordTextbox = "input[name='password']";
    public final static String googleIdContinueButton = "div[id='identifierNext']";
    public final static String googlePasswordContinueButton = "div[id='passwordNext']";
    public final static String logOutConfirmationButton = "input[id='logout-submit']";

    //Global COnfluence Menu
    public final static String createPageButton = "div[data-test-id='global-create-page-button']";
    public final static String spacesButton = "//span[contains(text(),'Spaces')]";
    public final static String YourWorkButton = "//span[contains(text(),'Your work')]";
    public final static String systemUserButton = "div[data-webitem-location='system.user'] button";
    public final static String logOutButton = "a[content = 'Log Out']";
    public final static String confluenceTitle = "svg[title = 'Confluence']";

    //Create modal locators
    public final static String newBlankPageButton = "li[data-item-module-complete-key='com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blank-page']";
    public final static String createModalButton = "button[data-test-id='create-dialog-create-button']";

    //Confluence Page locators
    public final static String confluencePageTitleInput = "input[data-test-id='tiny-content-title']";
    public final static String publishButton = "button[data-test-id='tiny-publish-button']";
    public final static String closeButton = "button[id = 'rte-button-cancel']";
    public final static String saveChangesIndicator = "div[id='pluggable-status']";
    public final static String errorAlertMessage = "div[id='aui-flag-container']";
    public final static String restrictionsIcon = "div[id='system-content-items']";
    public final static String editIcon = "a[id='editPageLink']";
    public final static String deleteConfirmationButton = "input[id='confirm']";

    //Space Left Menu Locators
    public final static String createdPagesList = "//div[contains(text(),'Pages')]/parent::div/following-sibling::div//a";
    public final static String firstCreatedPage = "//div[contains(text(),'Pages')]/parent::div/following-sibling::div//a[1]";
    public final static String firstPage = "//div[contains(text(),'Pages')]/parent::div/following-sibling::div//a[1]";

    //Space Directory Locators
    public final static  String QEHiringSpace = "a[title='QE Hiring']";

    //Your Work Locators
    public final static String draftsLink = "//div[contains(text(),'Drafts')]";
    public final static String createdDrafts = "//p[contains(text(),'Confluence')]";


    //Page Restrictions Modal Locators
    public final static String restrictionsTypeDropdown = "select[id='page-restrictions-dialog-selector']";
    public final static String applyRestrictionButton = "button[id='page-restrictions-dialog-save-button']";
    public final static String cancelRestrictionButton = "button[id='page-restrictions-dialog-close-button']";

    //PageEllipsisMenu
    public final static String pageEllipsisMenuButton = "div[data-test-id='view-page-ellipsis-menu]";
    public final static String deletePageButton = "a[id='action-remove-content-link']";

}
