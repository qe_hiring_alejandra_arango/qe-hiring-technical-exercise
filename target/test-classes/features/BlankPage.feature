@blankPage
Feature: Create and modify blank page on Confluence
 @createPageScenario
 Scenario: A page should appear on space pages section when the user creates and publishes a new blank page
    Given An user logged into Confluence space
    When  The user creates and publishes a new blank page
    Then  The page should appear on the space pages section
  @createPageScenario
 Scenario: An error message should appear when the user tries to publish a page without title
     Given An user logged into Confluence space
     When  The user creates and publishes a new blank page without a title
     Then  Confluence should show an error message
  @createPageScenario
 Scenario: A page should appear as draft on "My Work" section when an user creates and closes a new blank page
     Given An user logged into Confluence space
     When  The user creates and closes a new blank page
     Then  The draft should appear on 'My Work' section
 @restrictionsScenario
  Scenario: Everyone can view and edit a page with "No Restrictions" configuration.
    Given Exist a publish page on Confluence Space
    When  The user sets "No restrictions" for the page
    Then  Everyone can view and edit this page
  @restrictionsScenario
  Scenario: Everyone can view, only some can edit a page with "Editing restricted" configuration.
    Given Exist a publish page on Confluence Space
    When  The user sets "Editing restricted" for the page
    Then  Everyone can view only some can edit
  @restrictionsScenario @test
  Scenario: Only some people can view or edit. a page with "Viewing and editing restricted" configuration.
    Given Exist a publish page on Confluence Space
    When  The user sets "Viewing and editing restricted" for the page
    Then  Only some people can view or edit









