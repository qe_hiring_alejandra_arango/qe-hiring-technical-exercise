# QE Hiring - Technical Exercise

## Table of Contents

- [1. Challenge](#1-challenge)
  - [1.1. Description](#11-description)
   - [1.2. Assignment](#12-assignment)
- [2. Exploraty Testing Scenarios](#2-Exploraty-Testing-Scenarios)
    - [2.1. Found Bugs](#21-Found-Bugs)
- [3. Further Testing Areas](#3-Further-Testing-Areas)
- [4. Author](#author)

## 1. Challenge

### 1.1. Description

A graduate developer has written a new feature in Confluence called Reorder Pages. They have already tested that basic happy-path scenarios for sorting, dragging and dropping are working.
They are now asking for your help to determine whether the feature is ready for release to production. 

### 1.2. Assignment

Perform 30 minutes of exploratory testing of the feature to investigate if it is of high quality.
After you complete your testing, determine what further testing the feature may need before you would consider it ready to ship.

## 2. Exploraty Testing Scenarios

Considerations:
- Hierarchy Tree: Tree located on "Reordered Pages" tab
- Space Left Menu Tree: Tree located on the left Space Menu

Example Tree:

```
Hierarchy
 ┣ P1
 |  └── P2
 ┣ P3
|   └── P4
|        └── P5
 ┣ P6
 |  └── P7
 |  └── P8
 |  └── P9

  ```

- Tried to drag one of the father page to a child page (P1 inside P2)
- Deleted a father page in order to review that children pages changed their father automatically (delete P4 --> P5 should be child of P3)
- Change product page ´name´ to verify that position of the page didn't change (Change P7 to P11 --> P11 should not change of place)
- Tried to do a change on the "Space Left Menu Tree" and watch the behaviour of the "Hierarchy Tree" [Bug 1](#bug-1)
- Removed an unique child from his father (Removed P2 from p1) on the "Space Left Menu Tree" and without restart the page tried to expand the father on "Hierarchy Tree" (Expanded P1) [Bug 1](#bug-1)
- On the "Space Left Menu Tree" I moved a page to be child of other page (P8 child of P7)  and witout refresh the page I did the same scenario but in the othe way (P7 child of P8)
- Tried to perform the actions (order and drag) using right click
- Tried to change P6 folder order and after this action changed main folder order, this last action should only change the order of his children. 
- Deleted a child from "Space Left Menu Tree" (deleted P2) and tried to expand the father (Expand P1) on "Hierarchy Tree" [Bug 1](#bug-1)


### 2.1 Found bugs
 #### Bug 1
The changes on the "Space Left Menu Tree" should be shown automatically on the "Hierarchy Tree" and viceversa in order to avoid confusion for the final user

## 3. Further Testing Areas

This new component represents the hierarchy and order of all the pages that belong to a Confluence Space, for this reason I think it is so importat to execute a smoke test on the other components on Confluence.
I considered before the new change will be merged to production we need to consider some scenarios like:
1. What happen if a page doesn't have restrictions and we move it to a father with some restrictions.
2. What happen if two users change at the same tiem the hierarchy.
3. Whats happen if we do some changes on the order or in the heararchy while other user is editing one of the modified pages.

 As QA we need to have a good understanding of the business rules in order to prevent all the risk related with new functionalities.


## Author

[Alejandra Arango Arcila]