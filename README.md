# QE Hiring - Technical Exercise

![](Atlasian-QEHiring.gif)

## Table of Contents

- [1. Challenge](#1-challenge)
  - [1.1. Description](#11-description)
- [2. Stack](#2-stack)
- [3. Getting Started](#3-getting-started)
  - [3.1. Requirements](#31-requirements)
  - [3.2. Setup environment](#32-setup-environment)
- [4. Folder Structure](#4-folder-structure)
- [5. Notes](#5-notes)
- [6. Author](#author)

## 1. Challenge

### 1.1. Description

A team of developers is inexperienced with writing automated tests and have asked for your
help to get them started. The target of the testing is Confluence Cloud, Atlassian’s hosted wiki
product. To help them, write some example automated tests to verify that a user can:

- Create a new page.
- Set restrictions on an existing page. The developers will use your tests as a reference to
create their own, so be sure that your tests are robust, maintainable and valuable!

## 2. Stack

The project was build under above technologies

- Selenium v3.9.1
- Cucumber v1.2.2
- Java v9.0.4
- OS Windows 
- Chrome Driver v73.0.3683.20

## 3. Getting Started

### 3.1. Requirements

- [Maven](https://docs.wso2.com/display/IS323/Installing+Apache+Maven+on+Windows)
- Standard Edition Development Kit (JDK™)

### 3.2. Setup environment

1. Check that the system meet the requirements.
2. Clone repository from ´https://qe_hiring_alejandra_arango@bitbucket.org/qe_hiring_alejandra_arango/qe-hiring-technical-exercise.git´
3. Run ´mvn -Dtest=CucumberRunner test´


```bash
$ git clone https://qe_hiring_alejandra_arango@bitbucket.org/qe_hiring_alejandra_arango/qe-hiring-technical-exercise.git
$ cd qe-hiring-technical-exercise
$ mvn -Dtest=CucumberRunner test
```

## 4. Folder Structure

```
QEHiring
 ┣ data_pool
 |   └── Data.java
 ┣ locators
  |   └── Locators.java
 ┣ pages
  |   └── ConfluencePage.java
  |   └── ConfluenceSpaceDirectory.java
  |   └── ConfluenceSpaceMenu.java
  |   └── CreateModal.java
  |   └── GlobalConfluenceMenu.java
  |   └── PageRestrictionsModal.java
  |   └── QEHiringPage.java
  |   └── SignIn.java
  |   └── YourWork.java
 ┣ steps_definition
  |   └── BlankPage.java
 ┣ support
  |   └── AutomationWebDriver.java
 ┣ cucumberRunner.java
 ┣ resources
  |   └── features
  |         └── BlankPage.feature
  |   └── ChromeDriver.exe
 ┣ README.md
```
An overview of what each of these does:

| FILE / DIRECTORY |      Description                                  |
|------------------|-------------------------------------------------|
| data_pool        | Aim of data needed on the project execution       | 
| locators         | Contains a java file with used test locators  | 
| pages            | Pages of Confluence site based on POM             | 
| steps_definition | Java method with an expression that links it to one or more Gherkin steps. | 
| support          | Automation Web Driver configuration and methods | 
| resources        | Chrome Driver and features based on BDD| 

## 5. Notes

For this project I decided to use BDD (Behaviour-Driven Developmen) in order to have clarity of every scenario that I decided to create for the exercise requirements. 
It is important to mention that for those tasks I wrote some examples in order to guide the development team, but we can add a lot of scenarios for those functionalities.

## Author

[Alejandra Arango Arcila]